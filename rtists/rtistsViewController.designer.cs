// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace rtists
{
	[Register ("rtistsViewController")]
	partial class rtistsViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel nameTxt { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel publishedLbl { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel publishedTxt { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView resultView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField searchTxt { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView spinner { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView summaryTxt { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel urlLbl { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel urlTxt { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel yearLbl { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel yearTxt { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (nameTxt != null) {
				nameTxt.Dispose ();
				nameTxt = null;
			}

			if (publishedLbl != null) {
				publishedLbl.Dispose ();
				publishedLbl = null;
			}

			if (publishedTxt != null) {
				publishedTxt.Dispose ();
				publishedTxt = null;
			}

			if (searchTxt != null) {
				searchTxt.Dispose ();
				searchTxt = null;
			}

			if (spinner != null) {
				spinner.Dispose ();
				spinner = null;
			}

			if (summaryTxt != null) {
				summaryTxt.Dispose ();
				summaryTxt = null;
			}

			if (urlLbl != null) {
				urlLbl.Dispose ();
				urlLbl = null;
			}

			if (urlTxt != null) {
				urlTxt.Dispose ();
				urlTxt = null;
			}

			if (yearLbl != null) {
				yearLbl.Dispose ();
				yearLbl = null;
			}

			if (yearTxt != null) {
				yearTxt.Dispose ();
				yearTxt = null;
			}

			if (resultView != null) {
				resultView.Dispose ();
				resultView = null;
			}
		}
	}
}
