﻿using System;

namespace rtists
{
	public class Artist
	{
		public Artist ()
		{
		}

		public string Name {
			get;
			set;
		}
		public string Mbid {
			get;
			set;
		}
		public string Url {
			get;
			set;
		}
		public Biography Bio {
			get;
			set;
		}
		public ArtistImageCollection Image {
			get;
			set;
		}
	}
}

