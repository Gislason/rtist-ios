﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using RestSharp;

namespace rtists
{
	public partial class rtistsViewController : UIViewController
	{
		private RestClient _restClient = new RestClient("http://ws.audioscrobbler.com");

		// ADD YOUR API-KEY HERE
		private string _apiKey = "";


		public rtistsViewController () : base ("rtistsViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			searchTxt.ShouldReturn += textField => {
				textField.ResignFirstResponder();
				spinner.StartAnimating();

				var request = new RestRequest("2.0/");
				request.AddParameter("method", "artist.getInfo");
				request.AddParameter("artist", searchTxt.Text);
				request.AddParameter("api_key", _apiKey);

				_restClient.ExecuteAsync<Artist> (request, response => {
					UpdateFieldsFromArtist (response.Data);
				});

				return true;
			};
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public void UpdateFieldsFromArtist(Artist artist)
		{
			InvokeOnMainThread (() => {
				Console.WriteLine(artist.Name);
				if (artist == null) return;
				resultView.Hidden = false;
				nameTxt.Text = artist.Name;
				yearTxt.Text = artist.Bio.YearFormed.ToString ();
				urlTxt.Text = artist.Url;
				publishedTxt.Text = artist.Bio.Published.ToString ();
				summaryTxt.Text = artist.Bio.Summary;
				spinner.StopAnimating ();
			});
		}
	}
}

